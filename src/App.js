import "../node_modules/bootstrap/dist/css/bootstrap.min.css"
import "../node_modules/bootstrap/dist/css/bootstrap.css"
import chaoMung from "./assets/images/download.png"
import like from "./assets/images/download.jpg"
function App() {
  return (
    <div className="container mt-5 text-center">
      <div className="row">
        <div className="col-12">
          <h1>
            Welcome to DevCamp 120!
          </h1>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <img src={chaoMung} alt="chaomung" />
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <label>
            Message for you for the next 12 months:
          </label>
        </div>
        <div className="col-12">
          <input type="text" className="form-control" placeholder="I feel so bad now, you look better!"></input>
        </div>
        <div className="col-12">
          <br></br>
          <button className="btn btn-success" style={{ width: "25%" }}>Send message</button>
        </div>
        <div className="col-12">
          <br></br>
          <p style={{ color: "blue" }}>I feel so bad now, you look better!</p>
          <img src={like} style={{ width: "10%" }} alt="like" />
        </div>
      </div>
    </div>
  );
}

export default App;
